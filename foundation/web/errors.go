package web

import "github.com/pkg/errors"

//FieldError = data validation error
type FieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

type ErrorResponse struct {
	Error  string       `json:"error"`
	Fields []FieldError `json:"fields,omitempty"`
}

//Error is used for error signaling (trusted)
type Error struct {
	Err    error
	Status int
	Fields []FieldError
}

//NewRequestError wraps a provided error with an HTTP status code.
//This function should be used when handlers encounter unexpected errors.
func NewRequestError(err error, status int) error {
	return &Error{err, status, nil}
}

//Error implements the error interface.
//It uses the default message of the wrapped error. This is what will be shown in the services' logs.
func (err *Error) Error() string {
	return err.Err.Error()
}

//shutdown is used for graceful termination of the services
type shutdown struct {
	Message string
}

func NewShutdownError(message string) error {
	return &shutdown{message}
}

func (s *shutdown) Error() string {
	return s.Message
}

func IsShutdown(err error) bool {
	if _, ok := errors.Cause(err).(*shutdown); ok {
		return true
	}
	return false
}
