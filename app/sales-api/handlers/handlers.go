package handlers

import (
	"bitbucket.org/djordjeandrejevic/service/foundation/web"
	"log"
	"net/http"
	"os"
)

func API(build string, shutdown chan os.Signal, log *log.Logger) *web.App {
	app := web.NewApp(shutdown)
	check := check{log: log}
	app.Handle(http.MethodGet, "/readiness", check.readiness)
	return app
}
